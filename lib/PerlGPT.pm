# COPYRIGHT
# ABSTRACT: PerlGPT LLM
# [[[ HEADER ]]]
#use RPerl;
package PerlGPT;
use strict;
use warnings;
our $VERSION = 0.001_000;

# [[[ OO INHERITANCE ]]]
use parent qw(Perl::Type::Class);
use Perl::Type::Class;

# [[[ OO PROPERTIES ]]]
our hashref $properties = {};

1;
