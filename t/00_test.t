# COPYRIGHT
use Test2::V0;
use Data::Dumper;

our $VERSION = 0.001_000;

plan tests => 3;

#diag '<<< DEBUG >>> have $foo = ', $foo, "\n";


ok ( 1, 'hard-coded truth value 1');
ok ( 2, 'hard-coded truth value 2');
ok ( 'howdy', 'hard-coded truth value \'howdy\'');


done_testing();

